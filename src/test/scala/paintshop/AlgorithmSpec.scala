package paintshop

import org.scalatest.{Matchers, WordSpec}

import scala.collection.immutable

class AlgorithmSpec extends WordSpec with Matchers {

  "Algorithm" should {

    "Solve simple example" in {
      val example = Example(5, List(
        Customer(List(Color.matte(1), Color.gloss(3), Color.gloss(5))),
        Customer(List(Color.gloss(2), Color.matte(3), Color.gloss(4))),
        Customer(List(Color.matte(5)))
      ))

      val solution = Solution(List(Color.gloss(1), Color.gloss(2), Color.gloss(3), Color.gloss(4), Color.matte(5)))

      Algorithm.solve(example) shouldBe Some(solution)
    }

    "Solve rich example" in {
      val example = Example(5, List(
        Customer(List(Color.matte(2))),
        Customer(List(Color.gloss(5))),
        Customer(List(Color.gloss(1))),
        Customer(List(Color.gloss(5), Color.gloss(1), Color.matte(4))),
        Customer(List(Color.gloss(3))),
        Customer(List(Color.gloss(5))),
        Customer(List(Color.gloss(3))),
        Customer(List(Color.matte(2))),
        Customer(List(Color.gloss(5), Color.gloss(1))),
        Customer(List(Color.matte(2))),
        Customer(List(Color.gloss(5))),
        Customer(List(Color.matte(4))),
        Customer(List(Color.gloss(5), Color.matte(4)))
      ))

      val solution = Solution(List(Color.gloss(1), Color.matte(2), Color.gloss(3), Color.matte(4), Color.gloss(5)))

      Algorithm.solve(example) shouldBe Some(solution)
    }

    "Return no solution" in {
      val example = Example(1, List(
        Customer(List(Color.matte(1))),
        Customer(List(Color.gloss(1)))
      ))

      Algorithm.solve(example) shouldBe None
    }

    "Find worst case solution" in {
      val nColors = 100
      val nCustomers = 10000

      val customers = (1 to nCustomers).map { i =>
        val mattePos = i % nColors + 1
        Customer((1 to mattePos).map(j => Color(j, j == mattePos)).toList)
      }

      val example = Example(nColors, customers)
      val solution = Solution((1 to nColors).map(i => Color(i, isMatte = true)).toList)

      // in this example algorithm has to iterate over customers for each color due to matte colors special choice
      Algorithm.solve(example) shouldBe Some(solution)
    }
  }
}
