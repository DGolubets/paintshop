package paintshop

import org.scalatest.{Matchers, WordSpec}

class IOSpec extends WordSpec with Matchers {

  "IO" should {

    "stringify solution" in {
      val solution = Solution(List(Color.gloss(1), Color.matte(2), Color.gloss(3), Color.gloss(4), Color.matte(5)))
      IO.stringifySolution(solution) shouldBe "G M G G M"
    }

    "parse example" in {

      val lines = List(
        "5",
        "1 M 3 G 5 G",
        "2 G 3 M 4 G",
        "5 M"
      )

      val expectedExample = Example(5, List(
        Customer(
          List(
            Color.matte(1),
            Color.gloss(3),
            Color.gloss(5)
          )),
        Customer(List(
          Color.gloss(2),
          Color.matte(3),
          Color.gloss(4)
        )),
        Customer(List(
          Color.matte(5)
        ))
      ))

      val parsedExample = IO.parseExampleFromLines(lines)

      parsedExample.colorsNumber shouldBe expectedExample.colorsNumber
      parsedExample.customers.toList shouldBe expectedExample.customers.toList
    }
  }
}
