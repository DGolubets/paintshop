package paintshop

import scala.collection.immutable

/**
  * An example to solve
  *
  * @param colorsNumber number of colors defined
  * @param customers    customers list
  */
case class Example(colorsNumber: Int,
                   customers: immutable.Iterable[Customer])
