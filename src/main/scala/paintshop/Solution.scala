package paintshop

/**
  * Solution to an example problem
  *
  * @param colors colors that should be made
  */
case class Solution(colors: List[Color])