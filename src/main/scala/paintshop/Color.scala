package paintshop

case class Color(number: Int, isMatte: Boolean)

object Color {

  def gloss(number: Int): Color = Color(number, false)

  def matte(number: Int): Color = Color(number, true)
}
