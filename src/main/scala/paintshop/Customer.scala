package paintshop

/**
  * Describes a customer
  *
  * @param colors colors he\she likes
  */
case class Customer(colors: List[Color])
