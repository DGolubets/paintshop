package paintshop

import scala.annotation.tailrec

object Algorithm {

  /**
    * Searches for the cheapest colors to satisfy all customer preferences
    * with minimum number of matte colors (which are expensive).
    *
    * If some colors are not liked by anybody they will be made gloss.
    *
    * @param example example to solve
    * @return Some solution or None if not possible to solve
    */
  def solve(example: Example): Option[Solution] = {

    @tailrec
    def iteration(matteColors: Set[Int]): Option[Solution] = {

      val satisfaction = getSatisfaction(matteColors, example.customers)

      if (satisfaction.areSatisfied) {
        // everything is cool, return the solution
        val colors = (1 to example.colorsNumber).map { i =>
          Color(i, matteColors.contains(i))
        }
        Some(Solution(colors.toList))
      }
      else if (satisfaction.canBeSatisfied) {
        // try another iteration with more matte colors
        iteration(matteColors ++ satisfaction.mattesToAdd)
      }
      else {
        // can't satisfy everybody
        None
      }
    }

    // start with all gloss colors
    iteration(Set.empty)
  }

  private def getSatisfaction(matteColors: Set[Int], customers: Iterable[Customer]): Satisfaction = {

    @tailrec
    def iteration(satisfaction: Satisfaction, customers: Stream[Customer]): Satisfaction = {
      customers match {
        case customer #:: rest =>
          val (matte, gloss) = customer.colors.partition(_.isMatte)
          // customer is satisfied if his matte color is already in the list
          // or at least one of his gloss colors is not made matte
          val isSatisfied = matte.exists(c => matteColors.contains(c.number)) || gloss.exists(c => !matteColors.contains(c.number))

          if (isSatisfied) {
            // it's cool
            iteration(satisfaction, rest)
          }
          else if (matte.nonEmpty) {
            // we still can satisfy it, probably
            iteration(satisfaction.copy(areSatisfied = false, mattesToAdd = satisfaction.mattesToAdd ++ matte.map(_.number)), rest)
          }
          else {
            // can't be done
            satisfaction.copy(areSatisfied = false, canBeSatisfied = false)
          }
        case _ =>
          satisfaction
      }
    }

    iteration(Satisfaction(areSatisfied = true, canBeSatisfied = true, Set.empty), customers.toStream)
  }

  private case class Satisfaction(areSatisfied: Boolean,
                                  canBeSatisfied: Boolean,
                                  mattesToAdd: Set[Int])

}
