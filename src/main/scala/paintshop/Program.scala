package paintshop

import scala.util.{Failure, Success}

object Program extends App {
  args.toList match {
    case path :: Nil =>
      IO.readExampleFile(path).map(Algorithm.solve) match {
        case Success(Some(solution)) =>
          println(IO.stringifySolution(solution))
        case Success(None) =>
          println("No solution exists")
        case Failure(t) =>
          // I prefer to let it explode in main() and exit with non-zero code
          throw t
      }
    case _ =>
      println("Program requires exactly one argument: path to an example file.")
  }
}





