package paintshop

import java.io.File

import scala.collection.immutable
import scala.util.Try

object IO {

  /**
    * Read an Example from file
    *
    * @param path path to a file
    * @return example or error
    */
  def readExampleFile(path: String): Try[Example] = Try {
    parseExampleFromLines(fileLinesIterable(new File(path)))
  }

  def stringifySolution(solution: Solution): String = {
    solution.colors.map(c => s"${if (c.isMatte) "M" else "G"}").mkString(" ")
  }

  private[paintshop] def parseExampleFromLines(lines: immutable.Iterable[String]): Example = {
    val nColors = lines.head.toInt
    if (nColors <= 0) {
      throw new Exception(s"Invalid number of colors: $nColors")
    }

    val customers = new immutable.Iterable[Customer] {
      override def iterator: Iterator[Customer] = lines.iterator.drop(1).map(l => parseCustomer(l, nColors))
    }

    Example(nColors, customers)
  }

  private def parseCustomer(line: String, nColors: Int): Customer = {
    val colors = line.split(" ").grouped(2).map {
      case Array(col, attr) =>
        parseColor(col, attr, nColors)
      case other =>
        throw new Exception(s"Expected a color with attribute but found: ${other.mkString}")
    }.toList

    if (colors.count(_.isMatte) > 1) {
      throw new Exception("Customer cannot like more than one matte color!")
    }

    Customer(colors)
  }

  private def parseColor(number: String, attribute: String, nColors: Int): Color = {
    val colorNumber = number.toInt

    if (colorNumber <= 0 || colorNumber > nColors) {
      throw new Exception(s"Invalid color: $colorNumber")
    }

    val isMatte = attribute match {
      case "M" => true
      case "G" => false
      case other =>
        throw new Exception(s"Invalid color attribute: $other")
    }
    Color(colorNumber, isMatte)
  }


  /**
    * A special Iterable that will reopen file on every iteration,
    * but it allows not to buffer customers in memory and thus process big files.
    */
  private def fileLinesIterable(file: File): immutable.Iterable[String] = new immutable.Iterable[String] {
    override def iterator: Iterator[String] = {
      io.Source
        .fromFile(file)
        .getLines()
        .filter(_.trim.nonEmpty)
    }
  }
}
