# Paint Shop

## Usage

```
sbt "run somefile.txt"
```

or if you want to build jars:

```
sbt stage
target/universal/stage/bin/paintshop somefile.txt
```